﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlTestPrep.Tests
{
    using NUnit.Framework;
    using SqlTestPrep;

    [TestFixture]
    public class SettingsTests
    {
        [Test]
        public void Get_TestDBConnectionString_Success()
        {
            var result = Settings.TestDBConnectionString;

            Assert.That(result, Is.EqualTo("mydatabase"));
        }

        [Test]
        public void Get_SqlConfigurationFilePath_Success()
        {
            var result = Settings.SqlConfigurationFilePath;

            Assert.That(result, Is.EqualTo("mypath"));
        }
    }
}
