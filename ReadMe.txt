SQLTestPrep is designed to work with unit testing frameworks to test data access classes. Working with a clean database SQLTestPrep 
inserts a set of data into a database before your test and then deleting the data after the test. If you configure your unit test to 
run the SQLTestPrep before and after each test, each of your unit tests has a clean set of known data to manipulate. 

Setup is simple. 
1. Add a folder called "SqlConfiguration" to your unit test project. This folder will contain your .sql files for inserting and 
   deleting data from the database.
2. Add your .sql files to the SqlConfiguration folder
2. Add configuration file named "SqlConfiguration.json" to the root of your unit test project
3. Setup the configuration

[
  {
    "SqlConfigurationName": "",
    "ConnectionString": "",
    "SqlConfigurationJobs": [
      {
        "BeforeTestSqlFile": "",
        "AfterTestSqlFile": "",
        "SortOrder": ""
      },
      {
        "BeforeTestSqlFile": "",
        "AfterTestSqlFile": "",
        "SortOrder": ""
      }
    ]
  }
]

EXAMPLE

[
  {
    "SqlConfigurationName": "PersonTest",
    "ConnectionString": "Server=.;Database=Contact;User Id=contactuser;Password=password;",
    "SqlConfigurationJobs": [
      {
        "BeforeTestSqlFile": "C:\\repos\\SqlTestPrep\\SqlTestPrep.ScratchTests\\SqlConfiguration\\PersonTest_Setup.sql",
        "AfterTestSqlFile": "C:\\repos\\SqlTestPrep\\SqlTestPrep.ScratchTests\\SqlConfiguration\\PersonTest_Teardown.sql",
        "SortOrder": "0"
      },
      {
        "BeforeTestSqlFile": "C:\\repos\\SqlTestPrep\\SqlTestPrep.ScratchTests\\SqlConfiguration\\PersonTest_States_Setup.sql",
        "AfterTestSqlFile": "C:\\repos\\SqlTestPrep\\SqlTestPrep.ScratchTests\\SqlConfiguration\\PersonTest_States_Teardown.sql",
        "SortOrder": "1"
      }
    ]
  }
]

4. In your unit test create an instance of the SqlTestPrep.SqlLoader classes
5. Before each test call sqlLoader.Setup("<<SqlConfigurationName>>")
6. After each test call sqlLoader.Teardown("<<SqlConfigurationName>>")

For Example

//Instantiate SqlLoader
var sqlLoader = new SqlLoader();

//Call Setup before the test
sqlLoader.Setup("PersonTest");

//Call Teardown before the test
sqlLoader.Teardown("PersonTest");

