﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SqlTestPrep.ScratchTests
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var SqlLoader = new SqlLoader();

            Console.WriteLine("Setup Complete: {0}", SqlLoader.Setup("PersonTest"));

            Console.Read();

            Console.WriteLine("Teardown Complete: {0}", SqlLoader.TearDown("PersonTest"));

            Console.Read();
        }
    }
}
